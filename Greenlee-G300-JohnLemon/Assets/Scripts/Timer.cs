using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TMPro;
using System.Threading;

public class Timer : MonoBehaviour {

  public GameEnding gameEnding;
  public float timeRemaining = 240;
  public TextMeshProUGUI timerText;
  public bool timerIsRunning = false;

  private void Start() 
  {
    Thread.Sleep(7000);
    timerIsRunning = true;
  }

  private void Update() 
  {
    if (timerIsRunning)
    {
      if (timeRemaining > 0)
      {
        timeRemaining -= Time.deltaTime;
        DisplayTime(timeRemaining);
      }
      else
      {
        timerText.text = "Time has run out!";
        timeRemaining = 0;
        timerIsRunning = false;
        gameEnding.CaughtPlayer();
      }
    }
  }
  
  void DisplayTime(float timeToDisplay)
  {
    timeToDisplay += 1;
    float minutes = Mathf.FloorToInt(timeToDisplay / 60);
    float seconds = Mathf.FloorToInt(timeToDisplay % 60);
    timerText.text = String.Format("{0:00}:{1:00}", minutes, seconds);
  }
}
