using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Threading;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    Animator m_Animator;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    public Timer timer; 
    public TextMeshProUGUI invisibleText;
    public bool isInvisible;
    float timeToDisplay = 20;
    public TextMeshProUGUI instructionText;
    public Image blackBox;
    GameEnding ending;

    // Start is called before the first frame update
    void Start()
    {
        instructionText.text = "Welcome to John Lemon!" + "\n\n" + "Use the arrowkeys to move the character around" + "\n\n" + "The green cube pick-up item will add time" + "\n\n" + "The white sphere will temporarily make you invisible to enemies" + "\n\n" + "Avoid the gargoyles and ghosts while making your way to the church!" + "\n\n" + "Press any key to begin!";
        blackBox.enabled = true;
        Time.timeScale = 0;
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }
    void Update() {
        if(Input.anyKeyDown)
        {
            Time.timeScale = 1;
            instructionText.text = "";
            blackBox.enabled = false;
        }
        if (isInvisible)
        {
            timeToDisplay -= Time.deltaTime;
            DisplayInvisibleTime(timeToDisplay);
            if (timeToDisplay <= 0)
            {
                isInvisible = false;
                timeToDisplay = 20;
            }
        }
    }

    void DisplayInvisibleTime(float timeToDisplay)
  {
    if (timeToDisplay < 0)
    {
        invisibleText.text = "";
    }
    else
    {
        timeToDisplay += 1;
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        invisibleText.text = "Invisibility: " + string.Format("{0:00}:{1:00}", minutes, seconds);
    }
  }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if(isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.CompareTag("AddTime")) 
        {
            other.gameObject.SetActive(false);
            timer.timeRemaining += 30;
        }
        if(other.gameObject.CompareTag("Invisible"))
        {
            other.gameObject.SetActive(false);
            isInvisible = true;
        }
        if(other.gameObject.CompareTag("End"))
        {
            ending.EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
    }


    void OnAnimatorMove() 
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

}
